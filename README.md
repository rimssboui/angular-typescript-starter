# AngularTypescriptStarter

> This serves as a boilerplate starter angular project. This source code is inspired by best practices across the angular comunity and daily challenges that inforces to follow a pattern on creating newer Angular projects.

- **[Angular](https://angular.io/api/core)** (15.x)
- **[Angular CLI](https://github.com/angular/angular-cli)** (15.x)
- **[Typescript](https://www.typescriptlang.org/)** (4.x)
- **[TSlint](https://eslint.org/docs/latest/)** (6.x)

---

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Contribution

- The project can be forked to add feature and open a pull requests.
- It is possible to [Open](https://github.com/reemsb/angular-typescript-starter/issues/) an issue for bugs or feature request.
